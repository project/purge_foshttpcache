<?php

/**
 * @file
 * Contains \Drupal\purge_foshttpcache\Entity\HttpPurgerSettings.
 */

namespace Drupal\purge_foshttpcache\Entity;

use Drupal\purge\Plugin\Purge\Purger\PurgerSettingsBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerSettingsInterface;

/**
 * Defines the HTTP purger settings entity.
 *
 * @ConfigEntityType(
 *   id = "foshttpcachesettings",
 *   config_prefix = "settings",
 *   static_cache = TRUE,
 *   entity_keys = {"id" = "id"},
 * )
 */
class FOSHttpCacheSettings extends PurgerSettingsBase implements PurgerSettingsInterface {

  //
  // Instance metadata:
  //

  /**
   * The readable name of this purger.
   *
   * @var string
   */
  public $name = '';

  /**
   * The invalidation plugin ID that this purger invalidates.
   *
   * @var string
   */
  public $invalidationtype = 'tag';

  //
  // Primary backend information:
  //

  /**
   * The proxy type to connect to.
   *
   * @var string
   */
  public $proxytype = 'varnish';

  /**
   * The host or IP-address to connect to.
   *
   * @var string
   */
  public $hostname = 'localhost';

  /**
   * The port to connect to.
   *
   * @var int
   */
  public $port = 80;

  /**
   * The HTTP request method.
   *
   * @var string
   */
  public $request_method = 'BAN';

}
