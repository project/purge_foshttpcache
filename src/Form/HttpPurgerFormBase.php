<?php

/**
 * @file
 * Contains \Drupal\purge_foshttpcache\Form\FOSHttpCacheFormBase.
 */

namespace Drupal\purge_foshttpcache\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;
use Drupal\purge_foshttpcache\Entity\FOSHttpCacheSettings;

/**
 * Abstract form base for HTTP based configurable purgers.
 */
abstract class FOSHttpCacheFormBase extends PurgerConfigFormBase {

  /**
   * The service that generates invalidation objects on-demand.
   *
   * @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface
   */
  protected $purgeInvalidationFactory;

  /**
   * Static listing of all possible requests methods.
   *
   * @var array
   */
  protected $request_methods = ['BAN', 'PURGE', 'REFRESH'];

  /**
   * Static listing of all possible proxy types.
   *
   * @var array
   */
  protected $proxy_types = ['varnish', 'nginx'];

  /**
   * Constructs a \Drupal\purge_foshttpcache\Form\ConfigurationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface $purge_invalidation_factory
   *   The invalidation objects factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, InvalidationsServiceInterface $purge_invalidation_factory) {
    $this->setConfigFactory($config_factory);
    $this->purgeInvalidationFactory = $purge_invalidation_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('purge.invalidation.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'purge_foshttpcache.configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = FOSHttpCacheSettings::load($this->getId($form_state));
    $form['tabs'] = ['#type' => 'vertical_tabs', '#weight' => 10,];
    $this->buildFormMetadata($form, $form_state, $settings);
    $this->buildFormBackend($form, $form_state, $settings);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Build the 'metadata' section of the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\purge_foshttpcache\Entity\FOSHttpCacheSettings $settings
   *   Configuration entity for the purger being configured.
   */
  public function buildFormMetadata(array &$form, FormStateInterface $form_state, FOSHttpCacheSettings $settings) {
    $form['name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#description' => $this->t('A label that describes this purger.'),
      '#default_value' => $settings->name,
      '#required' => TRUE,
    ];
    $types = [];
    foreach ($this->purgeInvalidationFactory->getPlugins() as $type => $definition) {
      $types[$type] = (string)$definition['label'];
    }
    $form['invalidationtype'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#description' => $this->t('What sort of item will this purger clear?'),
      '#default_value' => $settings->invalidationtype,
      '#options' => $types,
      '#required' => FALSE,
    ];
  }

  /**
   * Build the 'backend' section of the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\purge_purger_http\Entity\HttpPurgerSettings $settings
   *   Configuration entity for the purger being configured.
   */
  public function buildFormBackend(array &$form, FormStateInterface $form_state, FOSHttpCacheSettings $settings) {
    $form['backend'] = [
      '#type' => 'details',
      '#group' => 'tabs',
      '#title' => $this->t('Backend'),
      '#description' => $this->t('In this section you configure the backend.')
    ];
    $form['backend']['hostname'] = [
      '#title' => $this->t('Hostname'),
      '#type' => 'textfield',
      '#default_value' => $settings->hostname,
    ];
    $form['backend']['port'] = [
      '#title' => $this->t('Port'),
      '#type' => 'textfield',
      '#default_value' => $settings->port,
    ];
    $form['backend']['request_method'] = [
      '#title' => $this->t('Request Method'),
      '#type' => 'select',
      '#default_value' => array_search($settings->request_method, $this->request_methods),
      '#options' => $this->request_methods,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $settings = FOSHttpCacheSettings::load($this->getId($form_state));

    // Rewrite 'request_method' to have the right CMI value.
    if (!is_null($method = $form_state->getValue('request_method'))) {
      $form_state->setValue('request_method', $this->request_methods[$method]);
    }

    // Iterate the config object and overwrite values found in the form state.
    foreach ($settings as $key => $default_value) {
      if (!is_null($value = $form_state->getValue($key))) {
        $settings->$key = $value;
      }
    }
    $settings->save();
  }

}
