<?php

/**
 * @file
 * Contains \Drupal\purge_foshttpcache\Form\FOSHttpCacheForm.
 */

namespace Drupal\purge_foshttpcache\Form;

use Drupal\purge_purger_http\Form\FOSHttpCacheFormBase;

/**
 * Configuration form for the FOSHttpCache.
 */
class FOSHttpCacheForm extends FOSHttpCacheFormBase {

  /**
   * The token group names this purger supports replacing tokens for.
   *
   * @see purge_tokens_token_info()
   *
   * @var string[]
   */
  protected $tokenGroups = ['invalidation'];

}
